[![DOI](https://zenodo.org/badge/doi/10.5281/zenodo.4687016.svg)](https://doi.org/10.5281/zenodo.4687016)

# Preliminary heated building gross volume density map of residential buildings in EU28 + Switzerland, Norway and Iceland for the year 2015


## Repository structure
```
data                    -- containts the dataset in Geotiff format
readme.md               -- Readme file 
datapackage.json        -- Includes the meta information of the dataset for processing and data integration

```


## Documentation
This dataset shows the heated building gross volume density map of residential buildings in EU28 on hectare (ha) level. 

For the spatial distribution of UED  and FEC  for SH we furthermore consider the surface-to-volume ratio of buildings and the share of building in different construction periods. For the surface-to-volume ratio, we build on the data derived from the OSM building layer: the building footprint and the estimated building height. For the share of buildings per construction period, we draw on soil sealing data for 1975, 1990, 2000 and 2014 on a 38 x 38 m raster [1], provided by the Global Human Settlement project. By considering the current share of soil sealed by buildings against the total share of sealed soil per grid cell (hectare level) as well as generic assumptions on building demolition, we derive the share of buildings per construction period for each grid cell.

**In the preliminary version of the heated volume density map, we presume a room height of 3 m for all buildings and use the heated gross floor areas.**

For detailed explanations and a graphical illustration of the dataset please see the [Hotmaps WP2 report](https://www.hotmaps-project.eu/wp-content/uploads/2018/03/D2.3-Hotmaps_for-upload_revised-final_.pdf) section 2.1.3 page 40.


### Limitations of the dataset
For the use and the estimation of the reliability of the data it is important to keep in mind that the data maps build on a statistical approach and do not take site specific or local conditions into account.


### References
[1] [GHS built-up grid, derived from Landsat, multitemporal (1975, 1990, 2000, 2014)](http://data.jrc.ec.europa.eu/dataset/jrc-ghsl-ghs_built_ldsmtdm_globe_r2015b)


## How to cite
Simon Pezzutto, Stefano Zambotti, Silvia Croce, Pietro Zambelli, Giulia Garegnani, Chiara Scaramuzzino, Ramón Pascual Pascuas, Alyona Zubaryeva, Franziska Haas, Dagmar Exner (EURAC), Andreas Mueller (e-think), Michael Hartner (TUW), Tobias Fleiter, Anna-Lena Klingler, Matthias Ku¨hnbach, Pia Manz, Simon Marwitz, Matthias Rehfeldt, Jan Steinbach, Eftim Popovski (Fraunhofer ISI) Reviewed by Lukas Kranzl, Sara Fritz (TUW)
Hotmaps Project, D2.3 WP2 Report – Open Data Set for the EU28, 2018 [www.hotmaps-project.eu](https://www.hotmaps-project.eu/wp-content/uploads/2018/03/D2.3-Hotmaps_for-upload_revised-final_.pdf) 


## Authors
Andreas Mueller <sup>*</sup>

<sup>*</sup> [TU Wien, EEG](https://eeg.tuwien.ac.at/), Institute of Energy Systems and Electrical Drives, Gusshausstrasse 27-29/370, 1040 Wien


## License
Copyright © 2016-2020: Andreas Mueller, Mostafa Fallahnejad
 
Creative Commons Attribution 4.0 International License
This work is licensed under a Creative Commons CC BY 4.0 International License.

SPDX-License-Identifier: CC-BY-4.0

License-Text: https://spdx.org/licenses/CC-BY-4.0.html

## Acknowledgment
We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) (Grant Agreement number 723677), which provided the funding to carry out the present investigation.

